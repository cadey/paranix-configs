{ config, pkgs, lib, ... }:

with lib;

let
  sources = import ../../nix/sources.nix;
  pkg = pkgs.callPackage sources.printerfacts { };
  cfg = config.xeserv.services.printerfacts;
in {
  options.xeserv.services.printerfacts = {
    enable = mkEnableOption "enable Printerfacts";
    useACME = mkEnableOption "enable ACME certs";

    domain = mkOption {
      type = types.str;
      default = "printerfacts.akua";
      example = "printerfacts.cetacean.club";
      description =
        "The domain name that nginx should check against for HTTP hostnames";
    };

    port = mkOption {
      type = types.int;
      default = 28318;
      example = 9001;
      description =
        "The port number printerfacts should listen on for HTTP traffic";
    };
  };

  config = mkIf cfg.enable {
    systemd.services.printerfacts = {
      wantedBy = [ "multi-user.target" ];

      script = ''
        export PORT=${toString cfg.port}
        export DOMAIN=${toString cfg.domain}
        export RUST_LOG=info
        exec ${pkg}/bin/printerfacts
      '';

      serviceConfig = {
        Restart = "always";
        RestartSec = "30s";
        WorkingDirectory = "${pkg}";
        RuntimeDirectory = "printerfacts";
        RuntimeDirectoryMode = "0755";
        StateDirectory = "tailscale";
        StateDirectoryMode = "0750";
        CacheDirectory = "tailscale";
        CacheDirectoryMode = "0750";
        DynamicUser = "yes";
      };
    };

    services.nginx.virtualHosts."${cfg.domain}" = {
      locations."/" = {
        proxyPass = "http://127.0.0.1:${toString cfg.port}";
        proxyWebsockets = true;
      };
      enableACME = cfg.useACME;
    };
  };
}
