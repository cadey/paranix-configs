{ config, lib, pkgs, ... }:

with lib;

let
  # Mara\ these options will be used for user account defaults in
  # the `mkUser` function.
  xeserv.users = {
    groups = mkOption {
      type = types.listOf types.str;
      default = [ "wheel" ];
      example = ''[ "wheel" "libvirtd" "docker" ]'';
      description =
        "The Unix groups that Xeserv staff users should be assigned to";
    };

    shell = mkOption {
      type = types.package;
      default = pkgs.bashInteractive;
      example = "pkgs.powershell";
      description =
        "The default shell that Xeserv staff users will be given by default.";
    };
  };

  cfg = config.xeserv.users;

  mkUser = { keys, shell ? cfg.shell, extraGroups ? cfg.groups, ... }: {
    isNormalUser = true;
    inherit extraGroups shell;
    openssh.authorizedKeys = {
      inherit keys;
    };
  };
in {
  options.xeserv.users = xeserv.users;

  config.users.users = {
    cadey = mkUser {
      keys = [
        "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIPg9gYKVglnO2HQodSJt4z4mNrUSUiyJQ7b+J798bwD9"
      ];
    };
    twi = mkUser {
      keys = [
        "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIPYr9hiLtDHgd6lZDgQMkJzvYeAXmePOrgFaWHAjJvNU"
      ];
    };
  };
}
