provider "aws" {
  region = "us-east-1"
}

terraform {
  backend "s3" {
    bucket = "xeserv-tf-state-paranoid"
    key    = "aws_image"
    region = "us-east-1"
  }
}

resource "aws_s3_bucket" "images" {
  bucket = "xeserv-ami-images"
  acl    = "private"

  tags = {
    Name = "Xeserv AMI Images"
  }
}

resource "aws_iam_role" "vmimport" {
  name               = "vmimport"
  assume_role_policy = file("./vmie-trust-policy.json")
}

resource "aws_iam_role_policy" "vmimport_policy" {
  name   = "vmimport"
  role   = aws_iam_role.vmimport.id
  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
        "s3:ListBucket",
        "s3:GetObject",
        "s3:GetBucketLocation"
      ],
      "Resource": [
        "${aws_s3_bucket.images.arn}",
        "${aws_s3_bucket.images.arn}/*"
      ]
    },
    {
      "Effect": "Allow",
      "Action": [
        "s3:GetBucketLocation",
        "s3:GetObject",
        "s3:ListBucket",
        "s3:PutObject",
        "s3:GetBucketAcl"
      ],
      "Resource": [
        "${aws_s3_bucket.images.arn}",
        "${aws_s3_bucket.images.arn}/*"
      ]
    },
    {
      "Effect": "Allow",
      "Action": [
        "ec2:ModifySnapshotAttribute",
        "ec2:CopySnapshot",
        "ec2:RegisterImage",
        "ec2:Describe*"
      ],
      "Resource": "*"
    }
  ]
    }
EOF
}

resource "aws_s3_bucket_object" "nixos_21_05" {
  bucket = aws_s3_bucket.images.bucket
  key    = "nixos-21.05-paranoid.vhd"

  source = "./result/nixos.vhd"
  etag   = filemd5("./result/nixos.vhd")
}

resource "aws_ebs_snapshot_import" "nixos_21_05" {
  disk_container {
    format = "VHD"
    user_bucket {
      s3_bucket = aws_s3_bucket.images.bucket
      s3_key    = aws_s3_bucket_object.nixos_21_05.key
    }
  }

  role_name = aws_iam_role.vmimport.name

  tags = {
    Name = "NixOS-21.05"
  }
}

resource "aws_ami" "nixos_21_05" {
  name                = "nixos_21_05"
  architecture        = "x86_64"
  virtualization_type = "hvm"
  root_device_name    = "/dev/xvda"
  ena_support         = true
  sriov_net_support   = "simple"

  ebs_block_device {
    device_name           = "/dev/xvda"
    snapshot_id           = aws_ebs_snapshot_import.nixos_21_05.id
    volume_size           = 40 # you can go as low as 8 GB, but 40 is a nice number
    delete_on_termination = true
    volume_type           = "gp3"
  }
}

output "nixos_21_05_ami" {
  value = aws_ami.nixos_21_05.id
}
