let
  sources = import ../../nix/sources.nix;
  pkgs = import sources.nixpkgs { };
  system = "x86_64-linux";

  printerfacts = pkgs.callPackage sources.printerfacts { };

  configuration = { config, lib, pkgs, ... }: {
    imports = [
      ../../common
      "${sources.nixpkgs}/nixos/modules/virtualisation/amazon-image.nix"
    ];

    networking.firewall.allowedTCPPorts = [ 22 80 443 ];

    xeserv.paranoid.enable = true;

    services.nginx.enable = true;

    xeserv.services.printerfacts = {
      enable = true;
      domain = "3.237.88.228";
    };
  };
in import "${sources.nixpkgs}/nixos" { inherit system configuration; }
