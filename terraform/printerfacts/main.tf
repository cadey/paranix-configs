provider "aws" {
  region = "us-east-1"
}

terraform {
  backend "s3" {
    bucket = "xeserv-tf-state-paranoid"
    key    = "printerfacts"
    region = "us-east-1"
  }
}

data "terraform_remote_state" "aws_image" {
  backend = "s3"

  config = {
    bucket = "xeserv-tf-state-paranoid"
    key    = "aws_image"
    region = "us-east-1"
  }
}

resource "tls_private_key" "state_ssh_key" {
  algorithm = "RSA"
}

resource "aws_key_pair" "generated_key" {
  key_name   = "generated-key-${sha256(tls_private_key.state_ssh_key.public_key_openssh)}"
  public_key = tls_private_key.state_ssh_key.public_key_openssh
}

resource "aws_security_group" "printerfacts" {
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port   = -1
    to_port     = -1
    protocol    = "icmp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port   = -1
    to_port     = -1
    protocol    = "icmp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port   = 0
    to_port     = 65535
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port   = 0
    to_port     = 65535
    protocol    = "udp"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_instance" "printerfacts" {
  ami           = data.terraform_remote_state.aws_image.outputs.nixos_21_05_ami
  instance_type = "t3.micro"
  security_groups = [
    aws_security_group.printerfacts.name,
  ]
  key_name = aws_key_pair.generated_key.key_name

  root_block_device {
    volume_size = 40 # GiB
  }

  tags = {
    Name = "xe-printerfacts"
  }
}

output "printerfacts_public_ip" {
  value = aws_instance.printerfacts.public_ip
}

module "deploy_printerfacts" {
  source          = "git::https://github.com/Xe/terraform-nixos.git//deploy_nixos?ref=1b49f2c6b4e7537cca6dd6d7b530037ea81e8268"
  nixos_config    = "${path.module}/printerfacts.nix"
  hermetic        = true
  target_user     = "root"
  target_host     = aws_instance.printerfacts.public_ip
  ssh_private_key = tls_private_key.state_ssh_key.private_key_pem
  ssh_agent       = false
  build_on_target = false
}
