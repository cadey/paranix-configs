provider "aws" {
  region = "us-east-1"
}

resource "aws_s3_bucket" "bucket" {
  bucket = "xeserv-tf-state"
  acl    = "private"

  tags = {
    Name = "Terraform State"
  }
}
