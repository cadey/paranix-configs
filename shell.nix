{ pkgs ? import <nixpkgs> {} }:

pkgs.mkShell {
  buildInputs = with pkgs; [
    redo-apenwarr
    terraform
    niv

    bashInteractive
  ];
}
