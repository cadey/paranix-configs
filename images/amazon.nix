{ config, pkgs, lib, modulesPath, ... }:

{
  imports = [ ../common (modulesPath + "/virtualisation/amazon-image.nix") ];

  xeserv.paranoid.enable = true;
}

