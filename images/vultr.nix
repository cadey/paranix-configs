{ config, pkgs, lib, modulesPath, ... }:

{
  imports = [ ../common ../common/cloud/vultr.nix ];

  xeserv.paranoid.enable = true;
}

