{ format, configFile, system ? "x86_64-linux" }:

let
  sources = import ../nix/sources.nix;
  pkgs = import sources.nixpkgs { };
  config = (import "${sources.nixpkgs}/nixos/lib/eval-config.nix" {
    inherit system;
    modules = [ configFile ];
  });

in import ./make-image.nix {
  inherit (config) config pkgs;
  inherit (config.pkgs) lib;
  inherit format configFile;
}
